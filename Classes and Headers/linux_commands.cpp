// linux_commands.cpp
#include <iostream>
#include "Commands.h"
#include "Headers.h"

using namespace std;

int main()
{
	Headers headers;
	headers.main();
	
	Commands linux;
	linux.find();
	linux.locate();

	return 0;
}

