// Commands.cpp
#include <iostream>
#include "Commands.h"

using namespace std;

void Commands::find() {
	cout << endl;
	cout << "The find command can be used to find files and directories." << endl;
	cout << "The syntax is: # find ./dir -name <file.txt>" << endl;
	cout << endl;
}

void Commands::locate() {
	cout << endl;
	cout << "The locate command will provide you with the absolute path" << endl; 
	cout << "to a file." << endl;
	cout << "The syntax is: # locate <file.txt>" << endl;
	cout << endl;
}