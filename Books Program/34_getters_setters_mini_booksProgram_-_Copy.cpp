// 34_getters_setters_mini_booksProgram.cpp //

#include <iostream>
#include <Windows.h> // For windows only, to set color theme
#include "Books.h"
#include "Banners.h"

using namespace std;

int main()
{
	// Set color theme
	system("color 0B");

	// Print banner for program
	Banners banners;
	banners.main_banner();

	// Using the GET method
	Book book;
	cout << "Using the get method: " << endl;
	cout << book.getTitle() << endl;

	// Using the SET method
	cout << endl;
	cout << "Using the set method: " << endl;
	book.setTitle("Nietzche: Ubermensch");
	cout << book.toString() << endl;


	return 0;
}

