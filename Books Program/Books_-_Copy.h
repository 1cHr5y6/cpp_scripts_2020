// Books.h
#ifndef BOOKS_H_
#define BOOKS_H_

#include <iostream>

using namespace std;

class Book {
private:
	string title;
public:
	Book();
	string toString();
	void setTitle(string newTitle);
	string getTitle();
	
};
#endif /* BOOKS_H_ */ 
