// Books.cpp
#include <iostream>
#include "Books.h"

using namespace std;

Book::Book() {
	title = "Plato: The Republik"; 
}

string Book::toString() {
	return "The title of the book is: " + title;
}

// Setter method
void Book::setTitle(string newTitle) {
	title = newTitle;
}

// Getter method
string Book::getTitle() {
	return "The title of the book is: " + title;
}




