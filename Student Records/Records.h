// Records.h
#ifndef RECORDS_H_
#define RECORDS_H_

struct Student {
	short id;
	int  year;
	double gpa;
};

void print_student_record(Student student);


#endif /* RECORDS_H_ */ 
