// Records_separate_files.cpp
#include <iostream>
#include "Records.h"
#include "Banners.h"

using namespace std;

int main()
{
	Banners banners;
	banners.main();
	
	cout << "Student - Bob " << endl;
	Student bob{ 3893, 1, 3.9898 };
	print_student_record(bob);
	cout << endl;

	cout << "Student - Alice " << endl;
	Student alice{ 4932, 2, 3.9899 };
	print_student_record(alice);
	cout << endl;

	return 0;
}

