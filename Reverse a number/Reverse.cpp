// Reverse.cpp
#include <iostream>
#include "Reverse.h"

using namespace std;

void Reverse::reverse() {
	int n, reversedNumber = 0, remainder;

	cout << "Enter a whole number: " << flush;
	cin >> n;

	while (n != 0) {
		remainder = n % 10;
		reversedNumber = reversedNumber * 10 + remainder;
		n /= 10;
	}

	cout << "The reversed number is: " << reversedNumber << endl;
}