// reverse_number.cpp 
#include <iostream>
#include "Reverse.h"
#include "Header.h"

using namespace std;

int main()
{
	// Set color theme
	system("color 0B");

	// Banner
	Header main;
	main.header();

	// Function to reverse a number
	Reverse number;
	number.reverse();

	return 0;
}

// Reference:www.programiz.com/cpp-programming/examples/reverse-number

