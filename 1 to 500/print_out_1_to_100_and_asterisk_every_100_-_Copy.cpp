// print_out_1_to_100_and_asterisk_every_100.cpp
#include <iostream>

using namespace std;

int main()
{
	// Print out 1 to 100
	for (int count = 1; count <= 500; count++) {
		cout << count << endl;
		if (count % 100 == 0) {
			cout << "*" << endl;
			cout << endl;
		}
	}


	return 0;
}
