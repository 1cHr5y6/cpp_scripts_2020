#ifndef DAY_H // guard
#define DAY_H

// Avoid using namespace in headers,
// may cause silent name clashes between different libraries
// Reference: https://stackoverflow.com/questions/273978/c-how-to-declare-a-struct-in-a-header-file 


#include <string>
struct day {
	char day[10];
	int miles;

};

#endif
