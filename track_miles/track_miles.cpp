#include <iostream>
#include <limits>
#include "miles.h"

using namespace std;

int main() {
	day x;
	cout << "Enter the day of the week to track miles for: " << flush;
	cin >> x.day;
	cout << "Enter number of miles ran on " << x.day << ": " << flush;
	cin >> x.miles;

	cout << endl;
	cout << "_________________ Fitness Tracker ____________" << endl;
	cout << endl;
	cout << "Day: " << x.day << endl;
	cout << "Miles ran: " << x.miles << flush;
	cout << endl;
	cout << endl;
	cout << "Press any key to exit program." <<endl;

	// Code block to be used to prevent console from closing immediately
	// after program execution.
	cin.clear(); // Reset any error flags.
	// Ignore any characters in the input buffer until newline
	cin.ignore(numeric_limits<streamsize>::max(),'\n'); 
	cin.get(); // Wait for user input to exit

	return 0;

}
