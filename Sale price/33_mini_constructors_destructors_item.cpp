// 33_mini_constructors_destructors_item.cpp 

#include <iostream>
#include <Windows.h> // For windows only, to set color theme
#include <iomanip> // To set decimal out two places
#include "Price.h"
#include "Banners.h"
using namespace std;

int main()
{
	// Set color theme
	system("color 0B");

	Banners banners;
	banners.main_banner();

	Price price(10.00);

	// Get original price
	cout << "Original price is $" << fixed << setprecision(2) <<  price.getPrice() << endl;

	// Set sale price
	price.setPrice(7.00);
	cout << "Sale price is:    $" << fixed << setprecision(2) <<  price.getPrice() << endl;



	return 0;
}

