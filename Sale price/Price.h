// Price.h
#ifndef PRICE_H_
#define PRICE_H_

class Price {
private:
	double amount;
public: 
	void setPrice(double num);
	double getPrice(void); // Set sale price.
	Price(double num); // This is the constructor.
	~Price(); // This is the destructor.
};
#endif /* PRICE_H_ */ 
