// Price.cpp
#include <iostream>
#include "Price.h"

using namespace std;

Price::Price(double num) {
	amount = num;
}

Price::~Price() {
	cout << endl;
	cout << "You saved 30% off the retail price." << endl;
	cout << "Sale has ended. " << endl;
}

void Price::setPrice(double num) {
	amount = num;
}

double Price::getPrice(void) {
	return amount;
}




