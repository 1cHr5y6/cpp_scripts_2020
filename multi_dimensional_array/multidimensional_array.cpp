#include <iostream>
#include <Windows.h> // in order to set system color

using namespace std;

int main() {
	// Set color theme
	system("color 0B");

	int numbers[2][3] = {
		{1, 2, 3},
		{4, 5, 6}
	};

	cout << endl;
	cout << "C++ script -- multidimensional arrays" << endl;
	cout << "-------------------------------------" << endl;
	cout << endl;
	
	cout << "Total size of the numbers array: " << sizeof(numbers) << endl;
	cout << "Total size of each elment in the numbers arrays: " << sizeof(int) << endl;
	
	cout << endl;
	cout << "Numbers array:" << endl;
	cout << "--------------" << endl;
	for (int i =0; i < 2; i++ ) {
		for (int j = 0; j < 3; j++) {
			cout << numbers[i][j] << " " << flush;
		}
		cout << endl;

	}

	return 0;
}


